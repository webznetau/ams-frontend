const gulp = require('gulp');
const nunjucks = require('gulp-nunjucks');
var browserSync = require('browser-sync').create();
var nunjucksRender = require('gulp-nunjucks-render');
var sass = require('gulp-sass');
var plumber = require('gulp-plumber');



gulp.task('default', () =>
    gulp.src('src/index.html')
        .pipe(nunjucks.compile({title: 'nunjucks'}))
        .pipe(gulp.dest('dist'))
);

// Browser sync server from dist
gulp.task('bs', function() {
    browserSync.init({
        server: {
            baseDir: "./dist"
        }
    });
});



//compiling nunjucks 2 html pages
gulp.task('nunjucks', function() {

  // Gets .html and .nunjucks files in pages
  return gulp.src('src/*.html')

  // Renders template with nunjucks
  .pipe(nunjucksRender({
    path: 'src'
    }))

  // output files in app folder
  .pipe(gulp.dest('dist'))
});


//sass
gulp.task('sass', function() {
    return gulp.src("src/css/*.scss")
        .pipe(plumber())
        .pipe(sass())
        .pipe(gulp.dest("dist/assets/css"))
        .pipe(browserSync.stream());
});

// Watch
gulp.task('watch', ['bs', 'nunjucks'], function (){
    gulp.watch('src/css/*.scss', ['sass']); 

    gulp.watch('src/*.html', ['nunjucks']); 
    gulp.watch('src/**/*.html', ['nunjucks']); 
	gulp.watch('dist/js/**/*.js', browserSync.reload);
    gulp.watch('dist/*.html', browserSync.reload); 
    

});