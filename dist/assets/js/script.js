$(document).ready(function(){    
    let loader = $('#loader');
    
    setTimeout(function(){
        loader.fadeOut('slow');
    }, 500);
    
        
    $(window).on('beforeunload', function() {
        loader.show();
    });
});

new WOW().init();

$(document).ready(function(){
    let top_navbar = $(document).find('.top-navbar');
    let logo = top_navbar.find('.navbar-brand img');
    
    $('.navbar-toggler').click(function(){
        logo.toggle();
        top_navbar.toggleClass('opened');
    });
});

$('.top-navbar .nav-item').click(function(){
    let menu = $('.top-navbar');
    menu.find('.nav-item.active').removeClass('active');
    $(this).addClass('active');
});

$('.scrollTo').click(function(e){
    let href = $(this).attr('href');
    let has_hash = href && href.includes('#');   
    
    if(has_hash){        
        let hash = href.split('#')[1];
        let element = $(document).find('#'+hash);
        
        if(element.length > 0){
            e.preventDefault();
            element.get(0).scrollIntoView({behavior: 'smooth', block: 'start', inline: "start"});
        }
    }
});